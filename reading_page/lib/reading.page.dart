import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class ReadingPage extends StatefulWidget {
  static const routeName = "/reading";
  const ReadingPage({Key? key}) : super(key: key);

  @override
  _ReadingPageState createState() => _ReadingPageState();
}

class _ReadingPageState extends State<ReadingPage> {
  var leftColor = #ee9e00;
  // Danh sách hình ảnh (code cứng).
  List imageList = [
    Image.asset("assets/images/1.jpg"),
    Image.asset("assets/images/2.jpg"),
    Image.asset("assets/images/3.jpg"),
    Image.asset("assets/images/4.jpg"),
    Image.asset("assets/images/5.jpg"),
    Image.asset("assets/images/6.jpg"),
    Image.asset("assets/images/7.jpg"),
    Image.asset("assets/images/8.jpg"),
    Image.asset("assets/images/9.jpg"),
    Image.asset("assets/images/10.jpg"),
    Image.asset("assets/images/11.jpg"),
    Image.asset("assets/images/12.jpg"),
    Image.asset("assets/images/13.jpg"),
    Image.asset("assets/images/14.jpg"),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.black,
          // backgroundColor: BoxDecoration(
          //   gradient: LinearGradient(
          //     colors: []
          //   )
          // ),
          leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: const Center(
            child: Text('Tên truyện'),
          ),
          actions: [
            IconButton(
                icon: const Icon(Icons.list),
                // Để tạm.
                onPressed: () => {
                  showMaterialModalBottomSheet(
                    context: context,
                    builder: (context) => Container(
                      height: 350.0,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(margin: EdgeInsets.only(top: 20.0),),
                          Text("Danh sách tập", textAlign: TextAlign.left, style: TextStyle(fontSize: 25.0),),
                          Container(margin: EdgeInsets.only(top: 20.0),),
                          Expanded(child:
                          ListView(
                            children: const <Widget>[
                              Card(
                                child: ListTile(
                                  leading: FlutterLogo(size: 56.0),
                                  title: Text('Two-line ListTile'),
                                  subtitle: Text('Here is a second line'),
                                  trailing: Icon(Icons.more_vert),
                                ),
                              ),
                              Card(
                                child: ListTile(
                                  leading: FlutterLogo(size: 56.0),
                                  title: Text('Two-line ListTile'),
                                  subtitle: Text('Here is a second line'),
                                  trailing: Icon(Icons.more_vert),
                                ),
                              ),
                              Card(
                                child: ListTile(
                                  leading: FlutterLogo(size: 56.0),
                                  title: Text('Two-line ListTile'),
                                  subtitle: Text('Here is a second line'),
                                  trailing: Icon(Icons.more_vert),
                                ),
                              ),
                              Card(
                                child: ListTile(
                                  leading: FlutterLogo(size: 56.0),
                                  title: Text('Two-line ListTile'),
                                  subtitle: Text('Here is a second line'),
                                  trailing: Icon(Icons.more_vert),
                                ),
                              ),
                              Card(
                                child: ListTile(
                                  leading: FlutterLogo(size: 56.0),
                                  title: Text('Two-line ListTile'),
                                  subtitle: Text('Here is a second line'),
                                  trailing: Icon(Icons.more_vert),
                                ),
                              ),
                            ],
                          ))
                        ],
                      ),
                    ),
                  )
                }
            )
          ]),
      body: Padding(
          padding: const EdgeInsets.fromLTRB(15, 15, 15, 20),
          child: ListView(scrollDirection: Axis.vertical, children: [
            // Looping images.
            for (var item in imageList)
              Center(
                child: Container(
                  margin: const EdgeInsets.all(4.0),
                  child: item,
                ),
              ),
          ])),
      bottomNavigationBar: BottomAppBar(
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            InkWell(
                onTap: () {

                },
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: const <Widget>[
                    Icon(Icons.arrow_back),
                    Text("Tập trước", style: TextStyle(fontWeight: FontWeight.bold)),
                  ],
                )
            ),
            const Text("Test", style: TextStyle(color: Colors.orange)),
            InkWell(
                onTap: () {

                },
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: const <Widget>[
                    Icon(Icons.arrow_forward),
                    Text("Tập sau", style: TextStyle(fontWeight: FontWeight.bold)),
                  ],
                )
            ),
          ],
        ),
      ),
    );
  }
}
