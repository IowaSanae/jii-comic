import 'dart:ui';
import 'dart:math' as math;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class SearchPage extends StatefulWidget {
  static const routeName = "/search";
  const SearchPage({Key? key}) : super(key: key);

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  // Danh sách hình ảnh (code cứng). Cùng kích cỡ với trong Figma
  List imageList = [
    Image.asset("assets/images/girls/img_1.jpeg", height: 160, width: 240),
    Image.asset("assets/images/girls/img_2.jpeg", height: 160, width: 240),
    Image.asset("assets/images/girls/img_3.jpeg", height: 160, width: 240),
    Image.asset("assets/images/girls/img_4.jpeg", height: 160, width: 240),
    Image.asset("assets/images/girls/img_5.jpeg", height: 160, width: 240),
    Image.asset("assets/images/girls/img_6.jpeg", height: 160, width: 240),
    Image.asset("assets/images/girls/img_7.jpeg", height: 160, width: 240),
    Image.asset("assets/images/girls/img_8.jpeg", height: 160, width: 240),
    Image.asset("assets/images/girls/img_9.jpeg", height: 160, width: 240),
    Image.asset("assets/images/girls/img_10.jpeg", height: 160, width: 240),
    Image.asset("assets/images/girls/img_11.jpeg", height: 160, width: 240),
    Image.asset("assets/images/girls/img_12.jpeg", height: 160, width: 240),
    Image.asset("assets/images/girls/img_13.jpeg", height: 160, width: 240),
    Image.asset("assets/images/girls/img_14.jpeg", height: 160, width: 240),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            flexibleSpace: Container(
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.bottomLeft,
                    end: Alignment.topRight,
                    colors: [
                      Color.fromRGBO(238, 157, 0, 1),
                      Color.fromRGBO(255, 0, 0, 1)
                    ],
                ),
              ),
            ),
            toolbarHeight: MediaQuery.of(context).size.height / 6,
            leading: IconButton(
              icon: const Icon(Icons.arrow_back_ios),
              // Code cứng vì alignment không di chuyển được thanh search vì lý do gì đó.
              padding: const EdgeInsets.only(left: 10.0),
              onPressed: () => Navigator.of(context).pop(),
            ),
            title: Container(
              width: 300,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
              ),
              height: 40,
              child: const TextField(
                decoration: InputDecoration(
                    // Tránh đường kẻ màu đen xuất hiện ở trên và dưới thanh search.
                    border: InputBorder.none,
                    hintText: "Search...",
                    hintStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                    ),
                    prefixIcon: Icon(
                      Icons.search,
                      color: Colors.black,
                    ),
                    suffixIcon: Icon(
                      Icons.clear_sharp,
                      color: Colors.black,
                    )),
              ),
            ),
            bottom: PreferredSize(
                preferredSize: const Size.fromHeight(0),
                child: Container(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 0, vertical: 16),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Container(
                            decoration: BoxDecoration(
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(20)),
                              border: Border.all(
                                color: const Color.fromRGBO(255, 255, 255, 1),
                                width: 1,
                              ),
                            ),
                            margin: const EdgeInsets.only(left: 10),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 16, vertical: 8),
                            child: InkWell(
                              onTap: () => Navigator.of(context).pop(),
                              child: const Text(
                                  "Action",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Color.fromRGBO(255, 255, 255, 1),
                                    fontFamily: 'Roboto',
                                    fontSize: 16,
                                    fontWeight: FontWeight.normal,
                                    height: 1,
                                  ),
                                ),
                            ),
                          ),
                          const SizedBox(width: 8),
                          Container(
                            decoration: BoxDecoration(
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(20)),
                              border: Border.all(
                                color: const Color.fromRGBO(255, 255, 255, 1),
                                width: 1,
                              ),
                            ),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 16, vertical: 8),
                            child: InkWell(
                              onTap: () => Navigator.of(context).pop(),
                              child: const Text(
                                  "Action",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Color.fromRGBO(255, 255, 255, 1),
                                    fontFamily: 'Roboto',
                                    fontSize: 16,
                                    fontWeight: FontWeight.normal,
                                    height: 1,
                                  ),
                                ),
                            ),
                          ),
                          const SizedBox(width: 8),
                          Container(
                            decoration: BoxDecoration(
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(20)),
                              border: Border.all(
                                color: const Color.fromRGBO(255, 255, 255, 1),
                                width: 1,
                              ),
                            ),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 16, vertical: 8),
                            child: InkWell(
                              onTap: () => Navigator.of(context).pop(),
                              child: const Text(
                                  "Action",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Color.fromRGBO(255, 255, 255, 1),
                                    fontFamily: 'Roboto',
                                    fontSize: 16,
                                    fontWeight: FontWeight.normal,
                                    height: 1,
                                  ),
                                ),
                            ),
                          ),
                          const SizedBox(width: 8),
                          Container(
                            decoration: BoxDecoration(
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(20)),
                              border: Border.all(
                                color: const Color.fromRGBO(255, 255, 255, 1),
                                width: 1,
                              ),
                            ),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 16, vertical: 8),
                            child: InkWell(
                              onTap: () => Navigator.of(context).pop(),
                              child: const Text(
                                  "Action",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Color.fromRGBO(255, 255, 255, 1),
                                    fontFamily: 'Roboto',
                                    fontSize: 16,
                                    fontWeight: FontWeight.normal,
                                    height: 1,
                                  ),
                                ),
                            ),
                          ),
                          const SizedBox(width: 8),
                        ])))),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              GridView.builder(
                  // dynamic height
                  shrinkWrap: true,
                  // Phải khai báo để tránh not in inclusive range.
                  itemCount: imageList.length,
                  gridDelegate:
                      const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2),
                  itemBuilder: (BuildContext context, int index) {
                    return Card(
                        child: GridTile(
                      child: imageList[index],
                    ),
                    margin: const EdgeInsets.symmetric(
                        horizontal: 16, vertical: 12));
                  })
            ],
          ),
        ));
  }
}
